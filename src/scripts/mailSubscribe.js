import $ from "jquery";
import { directive } from "@babel/types";

export function submit(data) {
  //validation

  const email = data.EmailID;

  if (email.length == 0) {
    alert("Please enter valid email address");
    return;
  }

  if (validateEmail(email)) {
  } else {
    alert("Invalid Email Address");
    return;
  }
  $("#disable3").attr("disabled", true);
  $.ajax({
    // url: "/script.php",
    url: "/scripts/sendemailSubscribe.php",

    type: "POST",
    data: {
      email: email
    },
    success: function(res) {
      $("#disable3").attr("disabled", false);
      // $(".subInput").val("");
      $("#inputClear")
        .find("input, textarea")
        .val("");
      // $("#City").val("");
      // $("#EmailID").val("");
      // $("#MobNumber").val("");
      // $("#Message").val("");
      alert("mail sent");
    },
    error: function(e) {
      $("#disable3").attr("disabled", false);

      alert("Error Occurred");
    }
  });
}

function validateEmail(email) {
  var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
  if (filter.test(email)) {
    return true;
  } else {
    return false;
  }
}
