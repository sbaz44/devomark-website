import React from "react";
// import { ReactDOM, hydrate, render } from "react-dom";
import ReactDOM from "react-dom";
import Main from "./jsx/Main";
import reducer from "./jsx/reducer";
import { Provider } from "react-redux";
import { createStore } from "redux";
const store = createStore(
  reducer /* preloadedState, */,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
ReactDOM.render(
  <Provider store={store}>
    <Main />
  </Provider>,
  document.getElementById("root")
);

// const rootElement = document.getElementById("root");
// if (rootElement.hasChildNodes()) {
//   hydrate(
//     <Provider store={store}>
//       <Main />
//     </Provider>,
//     rootElement
//   );
// } else {
//   render(
//     <Provider store={store}>
//       <Main />
//     </Provider>,
//     rootElement
//   );
// }
// "postbuild": "react-snap"
