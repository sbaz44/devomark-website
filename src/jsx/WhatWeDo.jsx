import React, { Component } from "react";
import Technology from "./Technology";
import Clients from "./Clients";

class WhatWeDo extends Component {
  state = {};

  render() {
    return (
      <React.Fragment>
        <Technology />
        <Clients />
      </React.Fragment>
    );
  }
}

export default WhatWeDo;
