import React, { Component } from "react";
import Background from "./Background";
import clientscss from "../css/clients.css";
import CaseStudy from "./CaseStudy";
import { entryanimation } from "./Animation";

class Clients extends Component {
  state = {};
  componentDidMount() {
    entryanimation("project", "01");
  }
  render() {
    return (
      <div className="client-container project02-container">
        <CaseStudy />
      </div>
    );
  }
}

export default Clients;
