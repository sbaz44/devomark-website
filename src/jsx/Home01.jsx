import React, { Component } from "react";
import "../css/home01.css";
import Background from "./Background";
import H1 from "./H1";
import Button from "./Button";
import P from "./P";
import { NavLink } from "react-router-dom";
import $ from "jquery";
import { createBrowserHistory } from "history";
const history = createBrowserHistory();
let bgimage =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563779261/Devomark/home01-background_utdklg.png";
let mbimage =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563779273/Devomark/mobbanner_tbpmum.png";
class Home01 extends Component {
  state = {};

  componentWillMount() {
    history.push("/home");
  }
  render() {
    const imageReplace = () => {
      if ($(window).width() < 800) {
        return mbimage;
      } else {
        return bgimage;
      }
    };
    return (
      <div className={"home01-container " + this.props.className}>
        <h1 style={{ display: "none" }}>Devomark</h1>

        <Background
          bgImg={imageReplace()}
          sectionNumber="01"
          activeLink={this.props.activeLink}
          linkcount={this.props.linkcount}
          description="Welcome to Devomark"
          arrowHidden={this.props.arrowHidden}
        />

        <div className={"panelContainer "}>
          <P className="home01-p">
            Devomark Enterprises Pvt. Ltd. is a broad based, multi-disciplinary,
            design and Development Company based in Mumbai, India. Our work has
            covered the gamut of media and styles - from the solidly corporate
            to the totally quirky.
          </P>
          <P className="home01-p">
            Our belief has always been in creating products and websites where
            good design meets strong code. Devomark leverages a wide range of
            skill sets to bring not only new perspectives to your brand, but
            also the newest techniques and technologies.
          </P>
          <NavLink style={{ textDecoration: "none" }} to="/contact">
            <Button className="customPosition01" text="Get A Quote" />
          </NavLink>
          <NavLink style={{ textDecoration: "none" }} to="/contact">
            <Button className="customPosition02" text="Partner With Us" />
          </NavLink>
        </div>
      </div>
    );
  }
}

export default Home01;
