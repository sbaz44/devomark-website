import React, { Component } from "react";
import "../css/background.css";
import Footer from "./Footer";
import Navlinks from "./Navlinks";
import Secondlinks from "./Secondlinks";
import $ from "jquery";
import { next } from "./Animation";
import { NavLink } from "react-router-dom";
let downarrow =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563779846/Devomark/downarrow_ogqox9.png";
let logo =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563779879/Devomark/logo_ebpczx.png";

class Background extends Component {
  state = {
    privacypolicy: function () {
      window.open('https://res.cloudinary.com/devomark/image/upload/v1575972073/privacypolicy_mjdamn.pdf');
    }
  };

  render() {
    const upperLayer = () => {
      return (
        <div className="upperLayer">
          <NavLink style={{ textDecoration: "none" }} to="/">
            <div className="logo">
              <img src={logo} alt="" className="img" />
            </div>
          </NavLink>
          <div className="quote-1">Make it happen</div>
          <div className="line quote-line" />
          <div>
            {(() => {
              if (this.props.description != "") {
                return (
                  <div className="quote2-container">
                    <div className="line quote2-line" />
                    <div className="quote-2">{this.props.description}</div>
                  </div>
                );
              }
            })()}
          </div>
          <div className="sectionNumber">{this.props.sectionNumber}</div>
          {this.props.arrowHidden == "true" || this.props.arrowHidden ? (
            <div />
          ) : (
              <div className="downArrow" onClick={() => next()}>
                <img className="img" src={downarrow} alt="" />
              </div>
            )}

          <Footer className="footer-custom" />
          <Navlinks activeLink={this.props.activeLink} />
          <Secondlinks
            activeLink={this.props.activeLink}
            activeSection={this.props.sectionNumber}
            linkcount={this.props.linkcount}
          />
          {(() => {
            if (this.props.sectionNumber === "06") {
              return <div className="privacyPolicyText" onClick={this.state.privacypolicy}>Privacy Policy</div>
            }
            else {
              return null
            }
          })()}
        </div>
      );
    };
    return (
      <div className={"backgroundContainer " + this.props.className}>
        <div className="leftPanel" />
        <div className="rightPanel">
          {this.props.bgImg ? (
            <img
              className="img"
              src={this.props.bgImg}
              alt="background Image"
            />
          ) : (
              <div />
            )}
        </div>
        <div>{upperLayer()}</div>
      </div>
    );
  }
}

export default Background;
