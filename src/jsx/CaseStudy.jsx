import React, { Component } from "react";
import "../css/casestudy.css";
import "../css/common.css";
import $ from "jquery";
import { divScroll } from "./Animation";
import Background from "./Background";
import { connect } from "react-redux";
let home =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563779988/Devomark/home_t1oxyc.png";

class CaseStudy extends Component {
  state = {};
  componentDidMount() {
    divScroll(".client-img");
  }
  renderClients = () => {
    return this.props.clients.map((item, index) => {
      if (item.client_name == this.props.activeClient) {
        return (
          <div className="client-cont">
            <div className="title-container">
              <div className="client-logo">
                <img className="client-logo" src={item.client_logo} alt="" />
              </div>
              <div className="client-name">{item.client_name}</div>
            </div>
            <div className="detail-container">
              <div className="client-img" id="clientimgg">
                <div className="client-images">
                  <img className="desk-img" src={item.client_img_desk} alt="" />
                  <img
                    className="mob-img"
                    src={item.client_img_mobile}
                    alt=""
                  />
                  <img className="desk-img" src={item.client_img_desk} alt="" />
                  <img
                    className="mob-img"
                    src={item.client_img_mobile}
                    alt=""
                  />
                </div>
              </div>

              <div className="info-container">
                <ul>
                  {item.client_work.map(work => (
                    <li>{work}</li>
                  ))}
                </ul>
              </div>
            </div>
          </div>
        );
      }
    });
  };
  render() {
    return (
      <React.Fragment>
        <Background
          sectionNumber="02"
          activeLink={this.props.activeLink}
          linkcount={this.props.linkcount}
        />
        <div className="case-study-container">
          {this.renderClients()}
          <div className="navigator">
            <div className="nav-name" onClick={() => this.props.prev()}>
              PREV
            </div>
            <img src={home} className="home-btn" />
            <div className="nav-name" onClick={() => this.props.next()}>
              NEXT
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => {
  return {
    clients: state.projects,
    activeClient: state.activeClient
  };
};

const mapDispatchToProps = dispatch => {
  return {
    next: () => dispatch({ type: "next" }),
    prev: () => dispatch({ type: "prev" })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CaseStudy);
