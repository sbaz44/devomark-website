import React, { Component } from "react";
import Background from "./Background";
import { NavLink } from "react-router-dom";
import H1 from "./H1";
import H2 from "./H2";
import P from "./P";
import DataContainer from "./DataContainer";
import SoftwareIcon from "./SoftwareIcon";
import Browse from "./Browse";
import Icons from "./Icons";
import BrowseData from "./BrowseData";
import SideNav from "./SideNav";
import Clients from "./Clients";
import { connect } from "react-redux";
import { entryanimation } from "./Animation";
import $ from "jquery";
let home =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563779988/Devomark/home_t1oxyc.png";

class Project01 extends Component {
  state = {
    work: this.props.work,
    workk: this.props.work

    // color: ["black", "yellow", "red", "blue"]
  };
  componentDidMount() {
    entryanimation("project", "01");
    this.props.handleClick(this.props.activeCategory);
  }
  componentDidUpdate() {
    entryanimation("project", "01");
  }

  render() {
    const renderTech = () => {
      return this.props.titles.map((data, index) => {
        if (data.title === this.props.work) {
          return (
            <React.Fragment key={index}>
              <Background
                sectionNumber="01"
                activeLink={this.props.activeLink}
                linkcount={this.props.linkcount}
                description={"Our Project"}
                arrowHidden={true}
              />
              <div className="navigator animated fadeInDown animation">
                <div
                  className="nav-name"
                  onClick={() => this.props.prevProject01()}
                >
                  PREV
                </div>
                <NavLink activeClassName="activeRoute" to="/" exact>
                  <img src={home} className="home-btn" />
                </NavLink>

                <div
                  className="nav-name"
                  onClick={() => this.props.nextProject01()}
                >
                  NEXT
                </div>
              </div>

              <DataContainer
                activeTitle={data.title}
                style={{ backgroundColor: data.color }}
              >
                {(() => {
                  if (data.title == "cms")
                    return <H1 className="title-text">CMS</H1>;
                  else if (data.title == "amc")
                    return <H1 className="title-text">AMC</H1>;
                  else if (data.title == "UI-UX")
                    return <H1 className="title-text">UI/UX</H1>;
                  else if (data.title == "APIDevelopment&Integration")
                    return (
                      <H1 className="title-text">
                        API Development & Integration
                      </H1>
                    );
                  else if (data.title == "FrontEndDevelopment")
                    return (
                      <H1 className="title-text">Front End Development</H1>
                    );
                  else return <H1 className="title-text">{data.title}</H1>;
                })()}

                <P className="para-text fadeInRight">{data.paragraph}</P>
                <SoftwareIcon className="animated fadeInDown animation">
                  {/* <H2 className="tech-text">Technologies</H2> */}
                  <Icons src={data.imageSRC} />
                </SoftwareIcon>
                <Browse>
                  <H2 className="browse-text">Browse</H2>
                  <BrowseData
                    links={this.props.links}
                    activeLink={data.title}
                  />
                </Browse>
              </DataContainer>
            </React.Fragment>
          );
        }
      });
    };

    return (
      <div
        className={
          "technology-container project01-container " + this.props.className
        }
      >
        {renderTech()}
        <div className="view-case">View Case Study</div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    work: state.category,
    links: state.links,
    titles: state.titles
  };
};

const mapDispatchToProps = dispatch => {
  return {
    nextProject01: () => dispatch({ type: "nextProject01" }),
    prevProject01: () => dispatch({ type: "prevProject01" }),
    handleClick: work => dispatch({ type: "handleClick", value: work })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Project01);
