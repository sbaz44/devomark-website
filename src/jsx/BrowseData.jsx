import React, { Component } from "react";
import "../css/browsedata.css";
import { connect } from "react-redux";
import { VerticalScroll } from "./Animation";
import Navlinks from "./Navlinks";
import { NavLink } from "react-router-dom";

class BrowseData extends Component {
  state = {};
  componentDidMount() {
    // VerticalScroll(".browsedata-container");
  }
  render() {
    return (
      <div className={"browsedata-container " + this.props.className}>
        {this.props.links.map((item, index) => {
          if (item === this.props.activeLink) {
            return (
              <div
                className="data-text orange"
                key={index}
                // onClick={() => this.props.renderBrowserr(item)}
              >
                {/* {item} */}
                {(() => {
                  if (item == "cms") return "Content Management System";
                  else if (item == "amc") return "Annual Maintenance Contract";
                  else if (item == "UI-UX") return "UI/UX";
                  else if (item == "APIDevelopment&Integration")
                    return "API Development & Integration";
                  else if (item == "FrontEndDevelopment")
                    return "Front End Development";
                  else return item;
                })()}
              </div>
            );
          } else {
            return (
              <NavLink to={"/whatwedo/" + item}>
                {(() => {
                  if (item == "cms")
                    return (
                      <div
                        className="data-text"
                        onClick={() => this.props.renderBrowserr(item)}
                        key={index}
                      >
                        Content Management System
                      </div>
                    );
                  else if (item == "amc")
                    return (
                      <div
                        className="data-text"
                        onClick={() => this.props.renderBrowserr(item)}
                        key={index}
                      >
                        Annual Maintenance Contract
                      </div>
                    );
                  else
                    return (
                      <div
                        className="data-text"
                        onClick={() => this.props.renderBrowserr(item)}
                        key={index}
                      >
                        {item}
                      </div>
                    );
                })()}

                {/* <div
                  className="data-text"
                  onClick={() => this.props.renderBrowserr(item)}
                  key={index}
                >
                  {item}
                </div> */}
              </NavLink>
            );
          }
        })}
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    renderBrowserr: name => dispatch({ type: "setBrowse", value: name })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BrowseData);
