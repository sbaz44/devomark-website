import React, { Component } from "react";
import DataContainercss from "../css/datacontainer.css";
import commoncss from "../css/common.css";

// import H1 from "./H1";
class DataContainer extends Component {
  state = {};
  render() {
    return (
      <div
        className={"data-container " + this.props.className}
        style={this.props.style}
      >
        {this.props.children}
      </div>
    );
  }
}

export default DataContainer;
