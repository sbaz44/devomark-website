import React, { Component } from "react";
import "../css/whyus02.css";
import Background from "./Background";
import ProcessContainer from "./ProcessContainer";
import Process from "./Process";
import H1 from "./H1";
import P from "./P";
import { VerticalScroll } from "./Animation";

class Whyus02 extends Component {
  state = {};
  componentDidMount() {
    VerticalScroll(".process-section");
  }
  render() {
    return (
      <div className={"whyus02-container " + this.props.className}>
        <Background
          sectionNumber="02"
          activeLink={this.props.activeLink}
          linkcount={this.props.linkcount}
          description="Our Process"
          arrowHidden={this.props.arrowHidden}
        />

        <div className="timeLine" />
        <div className="process-container">
          <H1 className="h1-adjust">Our Process</H1>
          <div className="process-section">
            <div className="process-1">
              <H1 className="customH1">Brief</H1>
              <P className="customP">
                At Devomark, we don’t start by designing a website we start by
                listening. Our first step is understanding you, your customers,
                and why you stand out from the competition. You won’t find any
                one-size-fits-all technology here, only custom-built solutions
                that reflect the things that make your business unique.
              </P>
              <div className="numberContainer number-adjust1">1</div>
            </div>
            <div className="process-2">
              <div className="numberContainer number-adjust2">2</div>
              <H1 className="customH1">Research</H1>
              <P className="customP">
                To plan and build the best possible website for your business,
                we need to get to know you. We kick things off with an
                introductory meeting and deep data analysis from your Google
                Analytics, SEO, and competitors. This report guides the choices
                to be made in your content strategy and web design.
              </P>
            </div>
            <div className="process-3">
              <H1 className="customH1">Analyse</H1>
              <P className="customP">
                Whether you have a clear specification or just a vague idea of
                what the website should look like we work with you to analyze
                and document the website functionality. During this phase we
                generate a strategic sitemap. This high-level planning tool
                serves as a blueprint for the overall structure of your site —
                what content you have, what you need, and where it will all go.
                Not a single requirement will miss our watchful eye.
              </P>
              <div className="numberContainer number-adjust3">3</div>
            </div>
            <div className="process-4">
              <div className="numberContainer number-adjust4">4</div>
              <H1 className="customH1">Execute</H1>
              <P className="customP">
                Armed with the knowledge of your business and a sound strategy,
                we proceed to the design and development of your site. Our
                graphic web design team will apply your creative and unique
                brand style to a custom designed website that is sure to take
                your breath away.
              </P>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Whyus02;
