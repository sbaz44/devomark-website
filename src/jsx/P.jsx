import React, { Component } from "react";
import "../css/p.css";

class P extends Component {
  constructor(props) {
    super(props);

    this.state = {
    };
  }



  render() {
    return (
      <p className={"customPara animated fadeInDown animation " + this.props.className}>
        {this.props.children}
      </p>
    );
  }
}

export default P;
