import React, { Component } from "react";
import "../css/footer.css";
let fb =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563780458/Devomark/facebook_msclbb.png";
let insta =
  "https://res.cloudinary.com/dubm0zyx2/image/upload/v1565844188/Devomark/instagram_ldmk2g_dlmcxg.png";
let linkden =
  "https://res.cloudinary.com/dubm0zyx2/image/upload/v1565844188/Devomark/linkedin_xlwn0o_l7ac7d.png";
class Footer extends Component {
  state = {};

  render() {
    return (
      <div className={"footer-container " + this.props.className}>
        <div className="info info-adjust">
          <a href="tel:8879568073">
            <span>+91</span>-8879568073
          </a>
        </div>
        <div className="info">
          <a href="mailto:info@devomark.com">
            <span>info</span>@devomark.com
          </a>
        </div>
        <div className="line line-footer" />
        <div className="social-container">
          <div className="social-logo">
            <a href="https://www.facebook.com/devomark.entps" target="_blank">
              <i className="fa fa-facebook img" aria-hidden="true" />
            </a>
          </div>
          <div className="social-logo">
            <a
              href="https://www.instagram.com/devomark_enterprises/"
              target="_blank"
            >
              <i className="fa fa-instagram img" aria-hidden="true" />
            </a>
          </div>
          <div className="social-logo">
            <a
              href="https://www.linkedin.com/company/14619924/"
              target="_blank"
            >
              <i className="fa fa-linkedin-square img" aria-hidden="true" />
            </a>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
