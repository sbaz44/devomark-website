import React, { Component } from "react";
let homeimg =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563779988/Devomark/home_t1oxyc.png";
class PrevNext extends Component {
  state = {};
  render() {
    return (
      <div className={"prevnext-container " + this.props.className}>
        <H2>Next</H2>
        <div className="home-img-container">
          <img src={homeimg} />
        </div>
        <H2>Prev</H2>
      </div>
    );
  }
}

export default PrevNext;
