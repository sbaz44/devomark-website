import React, { Component } from 'react';
import "../css/h2.css";
class H2 extends Component {
    state = {  }
    render() { 
        return ( 
            <h2 className={"heading2 " + this.props.className}>
            {this.props.children}
          </h2>
         );
    }
}
 
export default H2;