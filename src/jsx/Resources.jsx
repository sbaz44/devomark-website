import React, { Component } from "react";
import "../css/resource.css";
import Resources01 from "./Resources01";
import { entryanimation } from "./Animation";
import MenuButton from "./MenuButton";

class Home extends Component {
  state = {};

  componentDidMount() {
    entryanimation("resource", "01");
  }

  render() {
    return (
      <div className={" " + this.props.className}>
        <MenuButton />
        <Resources01 arrowHidden={true} activeLink="link3" linkcount={1} />
      </div>
    );
  }
}

export default Home;
