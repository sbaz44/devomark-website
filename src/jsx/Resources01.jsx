import React, { Component } from "react";
import "../css/resource01.css";
import Background from "./Background";
import DragContainer from "../jsx/DragContainer";
import DragComponents from "./DragComponents";
import { divScroll } from "./Animation";

let umair =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563454201/Devomark/Team/Umair.jpg";
let fahad =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563454213/Devomark/Team/Fahad.png";
let imaad =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563454213/Devomark/Team/Imaad.png";
let shahbaz =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563454213/Devomark/Team/Shahbaz.png";
let Mohammad =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563454213/Devomark/Team/Mohammad.png";
let Avish =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563454212/Devomark/Team/Avish.png";
let arfaat =
  "https://res.cloudinary.com/dubm0zyx2/image/upload/v1564749067/Devomark/Arfaat.jpg";

class Resources01 extends Component {
  state = {};
  componentDidMount() {
    divScroll(".team-cont");
  }
  render() {
    return (
      <div className={"resource01-container " + this.props.className}>
        <Background
          sectionNumber="01"
          activeLink={this.props.activeLink}
          linkcount={this.props.linkcount}
          description="Our Team"
          arrowHidden={this.props.arrowHidden}
        />
        <DragContainer
          h1Custom="h1textCustom"
          h1text="“Alone we can do so little; together we can do so much.”"
          ptext=""
        >
          <div className="team-cont">
            <div className="team-cont2">
              <DragComponents
                name="Arfat Sheikh"
                position="Co-Founder"
                img={arfaat}
                layout="resource01"
              />
              <DragComponents
                name="Imaad Damudi"
                position="Co-Founder"
                img={imaad}
                layout="resource01"
              />
              <DragComponents
                name="Avish Kadakia"
                position="Project Manager"
                img={Avish}
                layout="resource01"
              />
              <DragComponents
                name="Fahad Khan"
                position="Lead Front-End Developer"
                img={fahad}
                layout="resource01"
              />
              <DragComponents
                name="Mohammed Udaipurwala"
                position="Lead Back-End Developer"
                img={Mohammad}
                layout="resource01"
              />
              <DragComponents
                name="Umair Ansari"
                position="Front-End Developer"
                img={umair}
                layout="resource01"
              />
              <DragComponents
                name="Shahbaz Shaikh"
                position="Front-End Developer"
                img={shahbaz}
                layout="resource01"
              />
            </div>
          </div>
        </DragContainer>
      </div>
    );
  }
}

export default Resources01;
