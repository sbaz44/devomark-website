import React, { Component } from "react";
import MenuButtoncss from "../css/menubutton.css";
import $ from "jquery";
import { NavLink } from "react-router-dom";

class MenuButton extends Component {
  state = {};
  componentDidMount() {
    $("[data-toggle]").click(function() {
      //   alert("hi");
      var toggle_el = $(this).data("toggle");
      $(toggle_el).toggleClass("open-panel");
    });
    $(".menu-text").swipe({
      swipeStatus: function(
        event,
        phase,
        direction,
        distance,
        duration,
        fingers
      ) {
        if (phase == "move" && direction == "left") {
          $(".menu-container").removeClass("close-menu");
          $(".menu-container").addClass("open-menu");

          return false;
        }
      }
    });

    $(".menu-text").click(function() {
      $(".menu-container").removeClass("close-menu");
      $(".menu-container").addClass("open-menu");
    });
    $(".menu-container").swipe({
      swipeStatus: function(
        event,
        phase,
        direction,
        distance,
        duration,
        fingers
      ) {
        if (phase == "move" && direction == "right") {
          $(".menu-container").removeClass("open-menu");
          $(".menu-container").addClass("close-menu");
          return false;
        }
      }
    });

    $(".close-text").click(function() {
      $(".menu-container").removeClass("open-menu");
      $(".menu-container").addClass("close-menu");
    });
  }
  render() {
    return (
      <div className={"menubutton-container " + this.props.className}>
        <div className="menu-container">
          <div className="menu-text">
            <div data-toggle=".menubutton-container">MENU</div>
          </div>
          <div className="close-text">CLOSE</div>
          <ul>
            <NavLink activeClassName="activeRoute" to="/" exact>
              <li>Home</li>
            </NavLink>
            <NavLink activeClassName="activeRoute" to="/whyus">
              <li>Why Us</li>
            </NavLink>
            <NavLink activeClassName="activeRoute" to="/blog">
              <li>Blog</li>
            </NavLink>
            <NavLink activeClassName="activeRoute" to="/contact">
              <li>Get A Quote</li>
            </NavLink>
          </ul>
        </div>
      </div>
    );
  }
}

export default MenuButton;
