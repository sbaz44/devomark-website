import React, { Component } from "react";
import "../css/Secondlinks.css";
import { nextPage } from "./Animation";

class Secondlinks extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const inActiveLine = sectionNumber => {
      //      alert(sectionNumber);

      if (this.props.activeSection === sectionNumber) {
        return (
          <div className="active-line" key={sectionNumber}>
            <div className="line line-custom" />
            <div className="section-number">{sectionNumber}</div>
          </div>
        );
      } else {
        return (
          <div
            className={"lineContainer " + sectionNumber}
            onClick={() => nextPage(sectionNumber)}
            key={sectionNumber}
          >
            <div className="line line-custom" />
          </div>
        );
      }
    };
    const printLines = () => {
      //alert("called" + this.props.linkcount);
      let a = [];
      let i = 1;
      while (i <= this.props.linkcount) {
        a.push("0" + i);
        i = i + 1;
      }
      return a.map((item, i) => {
        return inActiveLine(item);
      });
    };
    return (
      <div className={"secondLinks " + this.props.className}>
        <div>{printLines()}</div>
      </div>
    );
  }
}

export default Secondlinks;
