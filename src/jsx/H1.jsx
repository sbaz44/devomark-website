import React, { Component } from "react";
import "../css/h1.css";

class H1 extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <h1
        className={
          "heading1 animated fadeInDown animation " + this.props.className
        }
      >
        {this.props.children}
      </h1>
    );
  }
}

export default H1;
