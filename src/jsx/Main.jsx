import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "./Home";
import Home01 from "./Home01";
import Home02 from "./Home02";
import Whyus from "./Whyus";
import Resources from "./Resources";
import DragContainer from "./DragContainer";
import Clients from "./Clients";
import "../css/common.css";
import WhatWeDo from "./WhatWeDo";
import Project from "./Project";
import Contact from "./Contact";
import Whyus02 from "./Whyus02";
import MenuButton from "./MenuButton";
import $ from "jquery";
import { NavLink } from "react-router-dom";
import Blog from "./Blog";
class Main extends Component {
  state = {};
  render() {
    const renderPage = params => {
      return alert(params.pagename);
    };
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/" component={Home} exact />
          <Route path="/home" component={Home} exact />
          <Route path="/offer" component={Home} exact />
          <Route path="/process" component={Home} exact />
          <Route path="/clients" component={Home} />
          <Route path="/locate" component={Home} />
          <Route path="/portfolio" component={Home} />

          <Route path="/whyus" component={Whyus} />
          <Route path="/blog" component={Blog} />
          <Route path="/contact" component={Contact} />
          {/* <Route path="/whatwedo" component={Project} /> */}
          <Route
            path="/whatwedo/:category"
            render={({ match }) => {
              return <Project activeCategory={match.params.category} />;
            }}
          />
        </Switch>
      </BrowserRouter>
    );
  }
}
export default Main;
