import React, { Component } from "react";
import "../css/home03.css";
import Background from "./Background";
import DragContainer from "../jsx/DragContainer";
import DragComponents from "./DragComponents";
import { entryanimation } from "./Animation";
import { NavLink } from "react-router-dom";

let analysisIcon =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563779526/Devomark/analysisIcon_lthbpv.png";
let searchIcon =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563779526/Devomark/searchIcon_imjiih.png";
let executeicon =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563779526/Devomark/executeicon_syezkl.png";
let exeicon =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563779526/Devomark/process_g6vcht.png";

let arrowRight =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563779706/Devomark/arrowicon_zidmhg.png";

class Home03 extends Component {
  state = {};
  componentDidMount() {
    const slider = document.querySelectorAll(".home03-cont");
    // const slider = $(".client-img");
    slider.forEach(function(node) {
      let isDown = false;
      let startX;
      let scrollLeft;
      node.addEventListener("mousedown", e => {
        isDown = true;
        // node.classList.add("activediv");
        startX = e.pageX - node.offsetLeft;
        scrollLeft = node.scrollLeft;
        node.style.cursor = "grabbing";
        // alert(isDown);
        console.log(isDown);
      });
      node.addEventListener("mouseleave", () => {
        isDown = false;
        node.style.cursor = "pointer";
      });
      node.addEventListener("mouseup", () => {
        isDown = false;
        node.style.cursor = "pointer";
      });
      node.addEventListener("mousemove", e => {
        if (!isDown) return;
        e.preventDefault();
        node.style.cursor = "grabbing";
        const x = e.pageX - node.offsetLeft;
        const walk = (x - startX) * 3; //scroll-fast
        node.scrollLeft = scrollLeft - walk;
        console.log(x);
      });
    });
  }
  render() {
    return (
      <div className={"home03-container " + this.props.className}>
        <Background
          sectionNumber="03"
          activeLink={this.props.activeLink}
          linkcount={this.props.linkcount}
          description="How We Work"
          arrowHidden={this.props.arrowHidden}
        />
        <DragContainer h1text="Our Process" ptext="">
          <div className="home03-cont animated fadeInDown animation">
            <div className="home03-cont2">
              <NavLink
                style={{ textDecoration: "none" }}
                to="/whyus"
                onClick={() => {
                  entryanimation("whyus", "02");
                }}
              >
                <DragComponents
                  serialNumber="01."
                  text="Brief"
                  img={analysisIcon}
                  layout="home03"
                />
              </NavLink>
              <div className="arrow">
                <img className="img" src={arrowRight} alt="" />
              </div>
              <NavLink
                style={{ textDecoration: "none" }}
                to="/whyus"
                onClick={() => {
                  entryanimation("whyus", "02");
                }}
              >
                <DragComponents
                  serialNumber="02."
                  text=" Research"
                  img={searchIcon}
                  layout="home03"
                />
              </NavLink>
              <div className="arrow">
                <img className="img" src={arrowRight} alt="" />
              </div>
              <NavLink
                style={{ textDecoration: "none" }}
                to="/whyus"
                onClick={() => {
                  entryanimation("whyus", "02");
                }}
              >
                <DragComponents
                  serialNumber="03."
                  text="Analyze"
                  img={executeicon}
                  layout="home03"
                />
              </NavLink>
              <div className="arrow">
                <img className="img" src={arrowRight} alt="" />
              </div>
              <NavLink
                style={{ textDecoration: "none" }}
                to="/whyus"
                onClick={() => {
                  entryanimation("whyus", "02");
                }}
              >
                <DragComponents
                  serialNumber="04."
                  text="Execute"
                  img={exeicon}
                  layout="home03"
                />
              </NavLink>
            </div>
          </div>
        </DragContainer>
      </div>
    );
  }
}

export default Home03;
