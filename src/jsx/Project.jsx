import React, { Component } from "react";
import Project01 from "./Project01";
import Project02 from "./Project02";
import { entryanimation, changeOpenClick } from "./Animation";
import "../css/project.css";
import MenuButton from "./MenuButton";
import { Helmet } from "react-helmet";
class Project extends Component {
  state = {};
  componentDidMount() {
    entryanimation("project", "01");
    // changeOpenClick();
  }
  render() {
    return (
      <div className={"project-container " + this.props.className}>
        <Helmet>
          <title>What We Do</title>
          <meta name="description" content="what we do" />
          <meta
            name="keywords"
            content="devomark,website,development,build,wordpress,html,css,api,android,backend,frontend,cms,amc"
          />
        </Helmet>
        <MenuButton />
        <Project01
          activeCategory={this.props.activeCategory}
          className="customCss1"
          activeLink="link1"
          linkcount="1"
        />
        {/* <Project02
          className="customCss2"
          activeLink="link1"
          linkcount="2"
          arrowHidden={true}
        /> */}
      </div>
    );
  }
}

export default Project;
