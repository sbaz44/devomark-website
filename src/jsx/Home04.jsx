import React, { Component } from "react";
import "../css/home04.css";
import Background from "./Background";
import DragContainer from "../jsx/DragContainer";
import DragComponents from "./DragComponents";
import { divScroll } from "./Animation";
import H1 from "./H1";

let Threeizone =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563617539/Devomark/Clients%20BW/3izone_j2wl3d.png";
let arjunrathi =
  "https://res.cloudinary.com/dubm0zyx2/image/upload/v1564832861/Devomark/Group_14_kgymx3.png";
let DK =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563617537/Devomark/Clients%20BW/Design_konstruct_hihwsc.png";
let HB =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563617539/Devomark/Clients%20BW/Herringbone_krchgo.png";
let IBH =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563617538/Devomark/Clients%20BW/IDH.png";
let maheshwari =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563617539/Devomark/Clients%20BW/Maheshwari_rlugpz.png";
let shahista =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563617539/Devomark/Clients%20BW/ShahistaRizvi_dwmm2k.png";
let abode =
  "https://res.cloudinary.com/dubm0zyx2/image/upload/v1564667659/Devomark/logo-abode-bombay_vowens.png";
let anandrathi =
  "https://res.cloudinary.com/dubm0zyx2/image/upload/v1564667660/Devomark/NoPath_-_Copy_j3uc3u.png";
let blettering =
  "https://res.cloudinary.com/dubm0zyx2/image/upload/v1564667659/Devomark/bombay_lettering_ggitdy.png";
let bora =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563617537/Devomark/Clients%20BW/bora-bora_mzduc4.png";
let china =
  "https://res.cloudinary.com/dubm0zyx2/image/upload/v1564667659/Devomark/china_gate_cyjb7g.png";
let doors =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563617539/Devomark/Clients%20BW/doors_nessvj.png";
let eventfaqs =
  "https://res.cloudinary.com/dubm0zyx2/image/upload/v1564667660/Devomark/eventfaqs-media-logo-red-new_zftumr.png";
let germanlaundry =
  "https://res.cloudinary.com/dubm0zyx2/image/upload/v1564667660/Devomark/Mask_Group_6_qye6io.png";
let gaintoct =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563617538/Devomark/Clients%20BW/giantoctopus_iygpzi.png";
let morplab =
  "https://res.cloudinary.com/dubm0zyx2/image/upload/v1564667659/Devomark/logo_1_yuntbn.png";
let muchhad =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563617538/Devomark/Clients%20BW/muchhad_w1kn3e.png";
let sambhav =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563617539/Devomark/Clients%20BW/sambhav_i0u700.png";
let shilpa =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563617538/Devomark/Clients%20BW/shillpa_purii_hf5gkf.png";
let trailblazers =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563617538/Devomark/Clients%20BW/traiblazers_f8zrwh.png";
let zoctr =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563617539/Devomark/Clients%20BW/zoctr_zgfhq7.png";

class Home04 extends Component {
  state = {};
  componentDidMount() {
    divScroll(".home04-cont");
  }
  render() {
    return (
      <div className={"home04-container " + this.props.className}>
        <Background
          sectionNumber="04"
          activeLink={this.props.activeLink}
          linkcount={this.props.linkcount}
          description="Our Clients"
          arrowHidden={this.props.arrowHidden}
        />
        <DragContainer h1text="Our Clients">
          {/* ptext="Like the multi brained multi tasker, we break down our projects into individual and independent modules and execute them, optimising the workflow. And we encourage our clients to do the same and join us in the process." */}
          <div className="home04-cont animated fadeInDown animation">
            <div className="home04-cont2">
              {/* <DragComponents img={Threeizone} layout="home04" /> */}
              <a href="https://rathibearings.com/" target="_blank">
                <DragComponents img={anandrathi} layout="home04" />
              </a>
              <a href="https://www.eventfaqs.com/" target="_blank">
                <DragComponents img={eventfaqs} layout="home04" />
              </a>
              <a href="http://chinagategroup.com/" target="_blank">
                <DragComponents img={china} layout="home04" />
              </a>
              <a href="https://www.germanlaundry.com" target="_blank">
                <DragComponents img={germanlaundry} layout="home04" />
              </a>
              <a href="https://arjunrathi.com" target="_blank">
                <DragComponents img={arjunrathi} layout="home04" />
              </a>

              <a href="http://morphlab.in/" target="_blank">
                <DragComponents img={morplab} layout="home04" />
              </a>
              <a href="https://abodeboutiquehotels.com/" target="_blank">
                <DragComponents img={abode} layout="home04" />
              </a>
              <a href="https://www.bombaylettering.com" target="_blank">
                <DragComponents img={blettering} layout="home04" />
              </a>

              {/* <DragComponents img={DK} layout="home04" /> */}
              {/* <DragComponents img={HB} layout="home04" /> */}
              {/* <DragComponents img={IBH} layout="home04" /> */}
              {/* <DragComponents img={maheshwari} layout="home04" /> */}
              {/* <DragComponents img={shahista} layout="home04" /> */}
              {/* <DragComponents img={bora} layout="home04" /> */}
              {/* <DragComponents img={doors} layout="home04" /> */}
              {/* <DragComponents img={gaintoct} layout="home04" /> */}
              {/* <DragComponents img={muchhad} layout="home04" /> */}
              {/* <DragComponents img={sambhav} layout="home04" /> */}
              {/* <DragComponents img={shilpa} layout="home04" /> */}
              {/* <DragComponents img={trailblazers} layout="home04" /> */}
              {/* <DragComponents img={zoctr} layout="home04" /> */}
            </div>
          </div>
        </DragContainer>
      </div>
    );
  }
}

export default Home04;
