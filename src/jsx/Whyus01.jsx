import React, { Component } from "react";
import "../css/whyus01.css";
import $ from "jquery";
import Background from "./Background";
import Button from "./Button";
import P from "./P";
import H1 from "./H1";
import { NavLink } from "react-router-dom";
let bgimage =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563781083/Devomark/whyus01_tqt0qy.png";
let mbimage =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563781082/Devomark/samuelmob_a2hmql.png";
class Whyus01 extends Component {
  state = {};

  render() {
    const imageReplace = () => {
      if ($(window).width() < 800) {
        return mbimage;
      } else {
        return bgimage;
      }
    };
    return (
      <div className={"whyus01-container " + this.props.className}>
        <Background
          bgImg={imageReplace()}
          sectionNumber="01"
          activeLink={this.props.activeLink}
          linkcount={this.props.linkcount}
          description="Why devomark"
          arrowHidden={this.props.arrowHidden}
        />
        <div className={"panelContainer "}>
          <H1 className="customPosition01">We Think in the Future</H1>
          <P>
            Devomark is <span>evolution</span>.
          </P>

          <P>
            Agile, adaptive and emotionally intelligent, creative problem
            solving is what we do.
          </P>

          <P>
            We pride ourselves in our ability to work with brands rather than
            for them. Together, we thrive amidst the constant change that
            surrounds us. With cognitive empathy at our very core, we convert a
            brands intent into action.
          </P>

          <NavLink style={{ textDecoration: "none" }} to="/contact">
            <Button className="customPosition02" text="Partner With Us" />
          </NavLink>
        </div>
      </div>
    );
  }
}

export default Whyus01;
