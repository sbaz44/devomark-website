import React, { Component } from "react";
import "../css/inputBox.css";
import $ from "jquery";
const clicked = a => {
  console.log(a);
  // if ($(this).val() < 1) {
  // $('label[for="' + a + '"]').css("visibility", "hidden");
  // } else if ($(this).val() < 0) {
  // $('label[for="' + a + '"]').css("visibility", "hidden");
  // }
  // $('label[for="' + a + '"]').css("visibility", "hidden");
};
class InputBox extends Component {
  render() {
    return (
      <div
        className={
          "inputBox animated fadeInDown animation " + this.props.className
        }
      >
        <input
          type="text"
          id={this.props.id}
          autoComplete="off"
          name={this.props.placeholder}
          className="field"
          required="required"
          value={this.props.value}
          onClick={() => clicked(this.props.placeholder)}
          // pattern="[0-9]*"
          onChange={this.props.Change}
        />
        <label htmlFor={this.props.placeholder}>{this.props.placeholder}</label>
      </div>
    );
  }
}

export default InputBox;
