import React, { Component } from "react";
import contactcss from "../css/contact.css";
import Background from "./Background";
import InputBox from "./InputBox";
import Button from "./Button";
import { entryanimation } from "./Animation";
import H1 from "./H1";
import MenuButton from "./MenuButton";
import { submit } from "../scripts/mail";
import { Helmet } from "react-helmet";

let data = {
  Fname: "test"
};
class Contact extends Component {
  state = {
    value: "",
    value2: "",
    value3: "",
    value4: "",
    value5: "",
    value6: ""
  };
  componentDidMount() {
    entryanimation("contact", "01");
  }
  handleChange = event => {
    this.setState({
      value: event.target.value
    });
  };

  handleChange2 = event => {
    this.setState({
      value2: event.target.value
    });
  };
  handleChange3 = event => {
    const value3 = event.target.validity.valid
      ? event.target.value
      : this.state.value3;

    this.setState({ value3 });
  };
  handleChange4 = event => {
    this.setState({
      value4: event.target.value
    });
  };
  handleChange5 = event => {
    this.setState({
      value5: event.target.value
    });
  };
  handleChange6 = event => {
    this.setState({
      value6: event.target.value
    });
  };
  render() {
    const sendDataToScript = e => {
      // e.preventDefault();
      data.Fname = this.state.value;
      data.EmailID = this.state.value2;
      data.MobNumber = this.state.value3;
      data.City = this.state.value4;
      data.Message = this.state.value5;
      data.Service = this.state.value6;

      submit(data);
    };

    return (
      <div className="contact01-container">
        <h1 style={{ display: "none" }}>Contact Us</h1>

        <Helmet>
          <title>Contact Us | Devomark </title>
          <link rel="canonical" href="https://www.devomark.com/contact" />
          <meta
            name="description"
            content="Connect with us at Devomark on +91-8879568073, Email Id-info@devomark.com. Please fill in your details we will get in touch with you."
          />
          <meta
            name="keywords"
            content="Web development company in south mumbai,Web development company in bandra/andheri,Best web devlopment company in mumbai,Website development"
          />

          <meta property="og:type" content="website" />
          <meta property="og:title" content=" devomark.entps" />
          <meta property="og:url" content=" https://www.devomark.com" />
          <meta
            property="og:description"
            content="Devomark is one of the best Website Designing and Web development company in India.We develop mobile responsive websites.We design customized Websites."
          />

          <meta name="twitter:card" content="summary" />
          <meta name="twitter:site" content=" @Devomart_ent" />
          <meta name="twitter:title" content=" Devomark" />
          <meta
            name="twitter:description"
            content=" Devomark is one of the best Website Designing and Web development company in India.We develop mobile responsive websites.We design customized Websites. "
          />
        </Helmet>
        <Background
          sectionNumber="01"
          activeLink="link4"
          // linkcount={1}
          description="Leave Us A Message"
          arrowHidden={true}
        />
        <MenuButton />
        <div className="inputBoxContainer" id="inputClear">
          <H1>
            Contact US<span className="orange">.</span>
          </H1>
          <InputBox
            type="text"
            placeholder="Name"
            id="Fname"
            value={this.props.value}
            Change={this.handleChange}
            onClick={this.props.onClick}
          />
          <InputBox
            type="email"
            placeholder="Email"
            id="EmailID"
            value={this.props.value2}
            Change={this.handleChange2}
          />
          <InputBox
            type="number"
            placeholder="Phone no."
            className="phone-adjust"
            id="MobNumber"
            value={this.props.value3}
            pattern="[0-9]*"
            Change={this.handleChange3}
          />
          <InputBox
            type="text"
            placeholder="City"
            className="city-adjust"
            id="City"
            value={this.props.value4}
            Change={this.handleChange4}
          />
          <select
            id="workstatus"
            value={this.state.value6}
            onChange={this.handleChange6}
            className="animated fadeInDown animation"
          >
            <option value="">Looking For</option>
            <option value="UI/UX">UI/UX</option>
            <option value="E-Commerce">E-Commerce</option>
            <option value="Front End Development">Front End Development</option>
            <option value="CMS">CMS</option>
            <option value="API Development & Integration">
              API Development & Integration
            </option>
            <option value="Artificial Intelligence">
              Artificial Intelligence
            </option>
            <option value="Back End Development">Back End Development</option>
            <option value="Annual Maintenance Contract">
              Annual Maintenance Contract
            </option>
            <option value="Web Hosting">Web Hosting</option>
          </select>

          {(() => {
            if (window.matchMedia("(max-width: 800px)").matches)
              return (
                <textarea
                  autoComplete="off"
                  name=""
                  id="Message"
                  cols="30"
                  rows="10"
                  placeholder="Message......."
                  className="message-area animated fadeInDown animation"
                  value={this.props.value5}
                  onChange={this.handleChange5}
                />
              );
            else
              return (
                <InputBox
                  type="text"
                  placeholder="Message"
                  className="Messages"
                  id="Message"
                  value={this.props.value5}
                  Change={this.handleChange5}
                />
              );
          })()}

          <Button
            className="customButton"
            text="Send"
            onClick={sendDataToScript}
            id="disable2"
          />
        </div>
      </div>
    );
  }
}

export default Contact;
