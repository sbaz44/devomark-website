import React, { Component } from "react";
import "../css/navlinks.css";
import { NavLink } from "react-router-dom";
import { nextPage } from "./Animation";

class Navlinks extends Component {
  state = {
    activeLink: this.props.activeLink
  };

  render() {
    return (
      <ul className={"Navlinks-container " + this.props.className}>
        <NavLink style={{ textDecoration: "none" }} to="/">
          <li
            className={
              "nav-link " +
              (this.state.activeLink === "link1"
                ? "active-link"
                : "inactive-link")
            }
          >
            Home
          </li>
        </NavLink>
        <NavLink style={{ textDecoration: "none" }} to="/whyus">
          <li
            className={
              "nav-link " +
              (this.state.activeLink === "link2"
                ? "active-link"
                : "inactive-link")
            }
          >
            Why Us
          </li>
        </NavLink>
        <NavLink style={{ textDecoration: "none" }} to="/blog">
          <li
            className={
              "nav-link " +
              (this.state.activeLink === "link3"
                ? "active-link"
                : "inactive-link")
            }
          >
            Blog
          </li>
        </NavLink>
        <NavLink style={{ textDecoration: "none" }} to="/contact">
          <li
            className={
              "nav-link " +
              (this.state.activeLink === "link4"
                ? "active-link"
                : "inactive-link")
            }
          >
            Get A Quote
          </li>
        </NavLink>
        {/* map() */}
        {/*<Technology >
          <H1>this.props.title</H1>
             <P>Para goes here</P>
            <SoftwareIcon>
              <H2>Technologies</H2>
              <Icons src={["ai.jpg","ps.jpg","xd.jpg"]}/>
                
            </SoftwareIcon>
            <Browse links={["UI/UX","E-commerce","FrontEnd"]} activelink="UI/UX" /> 
              
          </Technology>*/}
      </ul>
    );
  }
}

export default Navlinks;
