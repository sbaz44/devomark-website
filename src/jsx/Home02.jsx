import React, { Component } from "react";
import "../css/home02.css";
import Background from "./Background";
import DragContainer from "../jsx/DragContainer";
import DragComponents from "./DragComponents";
import $ from "jquery";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import { divScroll } from "./Animation";
import { createBrowserHistory } from "history";
const history = createBrowserHistory();
let cms =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563606248/Devomark/Technologies/CMS_jw6dlp.png";
let AI =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563605818/Devomark/Technologies/AI_i0lc2i.png";
let BED =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563606378/Devomark/Technologies/BED_eseaqn.png";
let AMC =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563524055/Devomark/Technologies/AMC_qkyshf.png";
let WebHosting =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563523787/Devomark/Technologies/WebHosting_y92hjb.png";
let uiuxicon =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563524218/Devomark/Technologies/uiuxicon_f9htuo.png";
let ecomicon =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563524219/Devomark/Technologies/ecomlogo_poaksz.png";
let frontendicon =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563524218/Devomark/Technologies/frontendicon_xqgb60.png";
let apiicon =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563524219/Devomark/Technologies/apiicon_lgnkhd.png";

class Home02 extends Component {
  state = {
    isClicked: true,
    work: ""
  };
  componentDidMount() {
    if ($(window).width() > 800) {
      divScroll(".home02-cont");
    }
  }

  componentWillMount() {
    // history.push("/home");
  }

  render() {
    const renderWork = () => {
      return (
        <React.Fragment>
          <Background
            sectionNumber="02"
            activeLink={this.props.activeLink}
            linkcount={this.props.linkcount}
            description="Our Portfolio"
            arrowHidden={this.props.arrowHidden}
          />
          {/* <div className="home02--cont" ></div> */}
          <DragContainer h1text="What We Offer" ptext="">
            <div className="home02-cont animated fadeInDown animation">
              <div className="home02-cont2">
                <NavLink
                  style={{ textDecoration: "none" }}
                  to="/whatwedo/UI-UX"
                  onClick={() => this.props.handleClick("UI-UX")}
                >
                  <DragComponents text="UI/UX" img={uiuxicon} layout="home02" />
                </NavLink>
                <NavLink
                  style={{ textDecoration: "none" }}
                  to="/whatwedo/E-Commerce"
                  onClick={() => this.props.handleClick("E-Commerce")}
                >
                  <DragComponents
                    text="E-Commerce"
                    img={ecomicon}
                    layout="home02"
                  />
                </NavLink>
                <NavLink
                  style={{ textDecoration: "none" }}
                  to="/whatwedo/FrontEndDevelopment"
                  onClick={() => this.props.handleClick("FrontEndDevelopment")}
                >
                  <DragComponents
                    text="Front End Development"
                    img={frontendicon}
                    layout="home02"
                  />
                </NavLink>
                <NavLink
                  style={{ textDecoration: "none" }}
                  to="/whatwedo/cms"
                  onClick={() =>
                    // this.props.handleClick("Content Management System")
                    this.props.handleClick("cms")
                  }
                >
                  <DragComponents text="CMS" img={cms} layout="home02" />
                </NavLink>
                <NavLink
                  style={{ textDecoration: "none" }}
                  to="/whatwedo/APIDevelopment&Integration"
                  onClick={() =>
                    this.props.handleClick("APIDevelopment&Integration")
                  }
                >
                  <DragComponents
                    text="API Development & Integration"
                    img={apiicon}
                    layout="home02"
                  />
                </NavLink>
                <NavLink
                  style={{ textDecoration: "none" }}
                  to="/whatwedo/Artificial Intelligence"
                  onClick={() =>
                    this.props.handleClick("Artificial Intelligence")
                  }
                >
                  <DragComponents
                    text="Artificial Intelligence"
                    img={AI}
                    layout="home02"
                  />
                </NavLink>
                <NavLink
                  style={{ textDecoration: "none" }}
                  to="/whatwedo/Back End Development"
                  onClick={() => this.props.handleClick("Back End Development")}
                >
                  <DragComponents
                    text="Back End Development"
                    img={BED}
                    layout="home02"
                  />
                </NavLink>
                <NavLink
                  style={{ textDecoration: "none" }}
                  to="/whatwedo/amc"
                  onClick={() => this.props.handleClick("amc")}
                >
                  <DragComponents text="AMC" img={AMC} layout="home02" />
                </NavLink>
                <NavLink
                  style={{ textDecoration: "none" }}
                  to="/whatwedo/Web Hosting"
                  onClick={() => this.props.handleClick("Web Hosting")}
                >
                  <DragComponents
                    text="Web Hosting"
                    img={WebHosting}
                    layout="home02"
                  />
                </NavLink>
              </div>
            </div>
          </DragContainer>
        </React.Fragment>
      );
    };
    return (
      <div className={"home02-container " + this.props.className}>
        {/* <Background
          sectionNumber="02"
          activeLink={this.props.activeLink}
          linkcount={this.props.linkcount}
          description="Our Portfolio"
          arrowHidden={this.props.arrowHidden}
        /> */}
        {renderWork()}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isClicked: state.isClicked
  };
};

const mapDispatchToProps = dispatch => {
  return {
    handleClick: work => dispatch({ type: "handleClick", value: work })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home02);
