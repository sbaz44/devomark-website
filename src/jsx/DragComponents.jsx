import React, { Component } from "react";
import "../css/dragComponents.css";
import "animate.css/animate.css";
class DragComponents extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {}
  render() {
    const getLayout = layout => {
      if (layout === "home02") {
        return (
          <div
            className={
              "home02 animated fadeInDown animation " + this.props.className
            }
          >
            <div className="icons" onClick={this.props.onClick}>
              <img className="img" src={this.props.img} alt="icon" />
            </div>
            <h4 className="text">{this.props.text}</h4>
          </div>
        );
      }
      if (layout === "home03") {
        return (
          <div
            className={"home03 shadow " + this.props.className}
            onClick={this.props.onClick}
          >
            <div className="srNo">{this.props.serialNumber}</div>
            <div className="icons">
              <img className="img" src={this.props.img} alt="icon" />
            </div>
            <h4 className="text">{this.props.text}</h4>
            <div className="bottom-line" />
          </div>
        );
      }
      if (layout === "home04") {
        return (
          <div className={"home04 shadow " + this.props.className}>
            <div className="icons">
              <img className="customImg" src={this.props.img} alt="icon" />
            </div>
          </div>
        );
      }
      if (layout === "resource01") {
        return (
          <div
            className={
              "resource01 animated fadeInDown animation " + this.props.className
            }
          >
            <div className="icons">
              <img className="img" src={this.props.img} alt="icon" />
            </div>
            <h4 className="name">{this.props.name}</h4>
            <p className="position">{this.props.position}</p>
          </div>
        );
      }
    };
    return (
      <div
        className={"animated fadeInDown animation " + this.props.className}
        onClick={this.props.onClick}
      >
        {getLayout(this.props.layout)}

        {/* <DragComponents text="UI/UX" img={uiuxicon} layout="home02" /> */}
      </div>
    );
  }
}

export default DragComponents;
