import React, { Component } from "react";
import "../css/processcontainer.css";
import H1 from "./H1";
class ProcessContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div className={"processContainer " + this.props.className}>
        <H1>Our Process</H1>
        <div className="timeLine"></div>
        {this.props.children}
      </div>
    );
  }
}

export default ProcessContainer;
