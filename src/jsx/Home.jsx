import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Helmet } from "react-helmet";
import "../css/home.css";
import $ from "jquery";
import Home01 from "./Home01";
import Home02 from "./Home02";
import Home03 from "./Home03";
import Home04 from "./Home04";
import Home05 from "./Home05";
import Home06 from "./Home06";
import SideNav from "./SideNav";
import MenuButton from "./MenuButton";
import { entryanimation } from "./Animation";
import { connect } from "react-redux";
// import Technology from "./Technology";
import Loading from "./Loading";
import { createBrowserHistory } from "history";
const history = createBrowserHistory();
class Home extends Component {
  state = {};

  componentDidMount() {
    entryanimation("home", "01");
    history.push("/home");
  }

  render() {
    return (
      <div className={"App " + this.props.className}>
        <Helmet>
          <title>Web Designing And Web Development Company | Devomark </title>
          <link rel="canonical" href="https://www.devomark.com" />
          <meta
            name="description"
            content="Devomark is one of the best Website Designing and Web development company in India.We develop mobile responsive websites.We design customized Websites."
          />
          <meta
            name="keywords"
            content="Web development in mumbai,custom designed websites,Web development companies in mumbai,Best web devlopment company in mumbai,Website development"
          />
          <meta property="og:type" content="website" />
          <meta property="og:title" content=" devomark.entps" />
          <meta property="og:url" content=" https://www.devomark.com" />
          <meta
            property="og:description"
            content="Devomark is one of the best Website Designing and Web development company in India.We develop mobile responsive websites.We design customized Websites."
          ></meta>

          <meta name="twitter:card" content="summary" />
          <meta name="twitter:site" content=" @Devomart_ent" />
          <meta name="twitter:title" content=" Devomark" />
          <meta
            name="twitter:description"
            content="Devomark is one of the best Website Designing and Web development company in India.We develop mobile responsive websites.We design customized Websites. "
          />
        </Helmet>
        {(() => {
          if (this.props.loading) {
            return (
              <div>
                <Loading />
              </div>
            );
          }
        })()}
        <SideNav activeLink="link1" />
        <MenuButton />
        <Home01 className="customCss1" activeLink="link1" linkcount={6} />
        <Home02 className="customCss2" activeLink="link1" linkcount={6} />
        <Home03 className="customCss3" activeLink="link1" linkcount={6} />
        <Home04 className="customCss4" activeLink="link1" linkcount={6} />
        <Home05 className="customCss5" activeLink="link1" linkcount={6} />
        <Home06
          className="customCss6"
          arrowHidden={true}
          activeLink="link1"
          linkcount={6}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.loading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setLoading: () => dispatch({ type: "set_loading" })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
