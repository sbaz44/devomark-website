import React, { Component } from "react";
import "../css/home05.css";
import Background from "./Background";
import LocationPanel from "./LocationPanel";
class Home05 extends Component {
  state = {

  };



  render() {
    return (
      <div className={
        "home05-container " + this.props.className
      }
      >
        <Background sectionNumber="05" activeLink={this.props.activeLink} linkcount={this.props.linkcount} description="Locate Us" arrowHidden={this.props.arrowHidden} />
        <LocationPanel className="animated fadeInDown animation" />
      </div>
    );
  }
}

export default Home05;
