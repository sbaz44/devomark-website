import React, { Component } from "react";
import "../css/home06.css";
import Background from "./Background";
import InputBox from "./InputBox";
import Button from "./Button";
import H1 from "./H1";
import { submit } from "../scripts/mailportfolio";
import $ from "jquery";
import { createBrowserHistory } from "history";
const history = createBrowserHistory();
let data = {
  Fname: "test"
};

class Home06 extends Component {
  state = {
    value: "",
    value2: "",
    value3: "",
    value4: "",
    value5: ""
  };
  componentDidUpdate() {}
  handleChange = event => {
    this.setState({
      value: event.target.value
    });
  };

  handleChange2 = event => {
    this.setState({
      value2: event.target.value
    });
  };
  handleChange3 = event => {
    const value3 = event.target.validity.valid
      ? event.target.value
      : this.state.value3;

    this.setState({ value3 });
  };
  handleChange4 = event => {
    this.setState({
      value4: event.target.value
    });
  };
  handleChange5 = event => {
    this.setState({
      value5: event.target.value
    });
  };
  // handleChange6 = event => {
  //   this.setState({
  //     value6: event.target.value
  //   });
  // };

  render() {
    const sendDataToScript = () => {
      // e.preventDefault();
      data.Fname = this.state.value;
      data.EmailID = this.state.value2;
      data.MobNumber = this.state.value3;
      data.City = this.state.value4;
      data.Message = this.state.value5;
      // data.Service = this.state.value6;
      submit(data);
    };

    return (
      <div className={"home06-container " + this.props.className}>
        <Background
          sectionNumber="06"
          activeLink="link1"
          linkcount={6}
          description="Leave Us A Message"
          arrowHidden={true}
        />

        <div className="inputBoxContainer" id="inputClear">
          <H1>
            Request Our Portfolio<span className="orange">.</span>
          </H1>
          <InputBox
            type="text"
            placeholder="Name"
            id="Fname"
            value={this.props.value}
            Change={this.handleChange}
          />
          <InputBox
            type="email"
            placeholder="Email"
            id="EmailID"
            value={this.props.value2}
            Change={this.handleChange2}
          />
          <InputBox
            type="number"
            placeholder="Phone no."
            className="phone-adjust"
            id="MobNumber"
            value={this.props.value3}
            pattern="[0-9]*"
            Change={this.handleChange3}
          />
          <InputBox
            type="text"
            placeholder="City"
            className="city-adjust"
            id="City"
            value={this.props.value4}
            Change={this.handleChange4}
          />

          {/* {(() => {
            if ($(window).width() < 800) {
              return ( */}
          <textarea
            name=""
            id="Message"
            cols="30"
            rows="2"
            placeholder="Message......."
            className="homeText-area animated fadeInDown animation"
            value={this.props.value5}
            onChange={this.handleChange5}
          />
          {/* );
            } else {
              return (
                <InputBox
                  type="text"
                  placeholder="Message"
                  className="homeMessages"
                  id="Message"
                  value={this.props.value5}
                  Change={this.handleChange5}
                />
              );
            }
          })()} */}

          <Button
            className="customButton"
            text="Send"
            id="disable"
            onClick={sendDataToScript}
          />
        </div>
      </div>
    );
  }
}

export default Home06;
