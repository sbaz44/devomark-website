import React, { Component } from "react";
import "../css/locationPanel.css";
import LocationInfo from "./LocationInfo";
let mumbaiImage =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563781154/Devomark/mumbaiImage_dlmaxj.png";

let contactlogo01 =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563780627/Devomark/contactlogo01_r2bq4s.png";
let contactlogo02 =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563780627/Devomark/contactlogo02_opgqlu.png";
let contactlogo03 =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563780627/Devomark/contactlogo03_zkgxba.png";
let contactlogo04 =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563780627/Devomark/contactlogo04_htuqwm.png";
let contactlogo05 =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563780627/Devomark/contactlogo05_ebhflj.png";

class LocationPanel extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div
        className={
          "panelContainer panelContainer-adjust " + this.props.className
        }
      >
        <div className="panelLeft">
          <div className="bgimg shadow">
            <img src={mumbaiImage} className="img custom-img" alt="" />
          </div>
          <h1 className="locationText">India, Mumbai</h1>
        </div>
        <div className="panelRight">
          {/* shadow */}
          <LocationInfo
            img={contactlogo01}
            text="Shalimar Building, Office no. 17"
          />
          <LocationInfo
            img={contactlogo02}
            text="Colaba Market opposite Silver House,Lala Nigam Rd, Apollo Bandar,Colaba, Mumbai - 400005"
          />
          <LocationInfo
            img={contactlogo03}
            maplink="https://maps.app.goo.gl/HAwmAWBwJ8PJJfYa6"
            text="Get Direction"
          />
          {/* <LocationInfo img={contactlogo04} text="+91-8879568073" />
          <LocationInfo img={contactlogo05} text="Info@devomark.com" /> */}
        </div>
      </div>
    );
  }
}

export default LocationPanel;
