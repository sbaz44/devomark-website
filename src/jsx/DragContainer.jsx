import React, { Component } from "react";
import "../css/dragContainer.css";

import P from "./P";
import H1 from "./H1";
class DragContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div className="panelContainer">
        {this.props.h1text !== "" ? (
          <H1 className={this.props.h1Custom}>{this.props.h1text}</H1>
        ) : (
          <div />
        )}
        {this.props.ptext !== "" ? (
          <P className="customP">{this.props.ptext}</P>
        ) : (
          <div />
        )}

        <div className={"dragContainer " + this.props.className}>
          {this.props.children}
        </div>
        <P className="grey">Drag To see more</P>
      </div>
    );
  }
}

export default DragContainer;
