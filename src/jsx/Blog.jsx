import Background from "./Background";
import React, { Component } from "react";
import { entryanimation, changeOpenClick } from "./Animation";
import H1 from "./H1";
import MenuButton from "./MenuButton";
import { Helmet } from "react-helmet";
import blogcss from "../css/blog.css";
class Blog extends Component {
  state = {};
  componentDidMount() {
    entryanimation("blog", "01");
    // changeOpenClick();
  }
  render() {
    return (
      <div className={"blog01-container " + this.props.className}>
        <h1 style={{ display: "none" }}>Coming soon</h1>

        <Helmet>
          <title>Technology Blog | Devomark </title>
          <link rel="canonical" href="https://www.devomark.com/blog" />
          <meta
            name="description"
            content="Blog on Technology,web desining,web development,web development in different languages and frameworks,UI/UX development,Custom Websites."
          />
          <meta
            name="keywords"
            content="Web development in react,UI/UX agency in mumbai,Content management system,CMS integration,Ecommerce website,Ui/ux development,Custom Websites."
          />

          <meta property="og:type" content="website" />
          <meta property="og:title" content=" devomark.entps" />
          <meta property="og:url" content=" https://www.devomark.com" />
          <meta
            property="og:description"
            content="Devomark is one of the best Website Designing and Web development company in India.We develop mobile responsive websites.We design customized Websites."
          />

          <meta name="twitter:card" content="summary" />
          <meta name="twitter:site" content=" @Devomart_ent" />
          <meta name="twitter:title" content=" Devomark" />
          <meta
            name="twitter:description"
            content=" Devomark is one of the best Website Designing and Web development company in India.We develop mobile responsive websites.We design customized Websites. "
          />
        </Helmet>
        <Background
          sectionNumber="01"
          activeLink="link3"
          linkcount="1"
          description="Blog"
          arrowHidden={true}
        />
        <MenuButton />
        <div className="panelContainer">
          <H1 className="h1Adjust">Coming Soon</H1>
        </div>
      </div>
    );
  }
}

export default Blog;
