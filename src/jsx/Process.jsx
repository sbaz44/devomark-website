import React, { Component } from "react";
import "../css/process.css";
import H1 from "./H1"
import P from "./P"
class process extends Component {
  constructor(props) {
    super(props);

    this.state = {
    };
  }



  render() {
    return (
      <div className={
        "process " + this.props.className
      }
      >
        <div className="textContainer">
          <H1 className="customH1">{this.props.title}</H1>
          <P className="customP">{this.props.content}</P>
        </div>
        {
          this.props.active ? <div className="numberContainer activeNumber">
            {this.props.number}
          </div>
            : <div className="numberContainer">
              {this.props.number}
            </div>
        }

      </div>
    );
  }
}

export default process;
