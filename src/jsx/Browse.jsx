import React, { Component } from "react";
import "../css/browse.css";
class Browse extends Component {
  state = {};
  render() {
    return (
      <div
        className={
          "browse-container animated slideInUp animation " +
          this.props.className
        }
      >
        {this.props.children}
      </div>
    );
  }
}

export default Browse;
