import React, { Component } from "react";
import "../css/whyus.css";
import Whyus01 from "./Whyus01";
import Whyus02 from "./Whyus02";
import { entryanimation } from "./Animation";
import MenuButton from "./MenuButton";
import { Helmet } from "react-helmet";
class Whyus extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {};
  }

  componentDidMount() {
    entryanimation("whyus", "01");
  }

  render() {
    return (
      <div className={"whyus-container " + this.props.className}>
        <h1 style={{ display: "none" }}>We Think in the Future</h1>
        <Helmet>
          <link rel="canonical" href="https://www.devomark.com/whyus" />
          <title>Best Web Development Company in Mumbai | Devomark </title>
          <meta
            name="keywords"
            content="Front end development,Api integration,Custom Ecommerce development,wordpress web development,Wix web development,Website maintainance"
          />
          <meta
            name="description"
            content="We have a team of experienced proffesionals. We assure to give you best quality services.We excel in developing  ecommerce,customized websites."
          />

          <meta property="og:type" content="website" />
          <meta property="og:title" content=" devomark.entps" />
          <meta property="og:url" content=" https://www.devomark.com" />
          <meta
            property="og:description"
            content="Devomark is one of the best Website Designing and Web development company in India.We develop mobile responsive websites.We design customized Websites."
          />

          <meta name="twitter:card" content="summary" />
          <meta name="twitter:site" content=" @Devomart_ent" />
          <meta name="twitter:title" content=" Devomark" />
          <meta
            name="twitter:description"
            content=" Devomark is one of the best Website Designing and Web development company in India.We develop mobile responsive websites.We design customized Websites. "
          />
        </Helmet>
        <MenuButton />
        <Whyus01 className="customCss1" activeLink="link2" linkcount="2" />
        <Whyus02
          className="customCss2"
          arrowHidden={true}
          activeLink="link2"
          linkcount="2"
        />
      </div>
    );
  }
}

export default Whyus;
