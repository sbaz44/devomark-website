import React, { Component } from "react";
import loadingcss from "../css/loading.css";
import { connect } from "react-redux";
import InputBox from "./InputBox";
import $ from "jquery";
import { submit } from "../scripts/mailSubscribe";
let data = {};
class Loading extends Component {
  state = { value: "" };
  componentDidMount() {
    // $("body").css("overflow-y", "hidden");
  }
  handleChange = event => {
    this.setState({
      value: event.target.value
    });
  };
  render() {
    const sendDataToScript = e => {
      data.EmailID = this.state.value;

      submit(data);
    };
    return (
      <React.Fragment>
        <div className="overlay" onClick={() => this.props.setLoading()} />
        <div className="subscribeContainer animated fadeIn animation">
          <div className="closeX" onClick={() => this.props.setLoading()}>
            X
          </div>
          <div className="inputContainer">
            <div className="subs-text">
              Stay up to date with our latest news and products.
            </div>
            <div className="subInput-container" id="inputClear">
              <input
                type="text"
                className="subInput"
                value={this.props.value}
                onChange={this.handleChange}
                autoComplete="off"
                placeholder="Your email address"
              />
              <button
                id="disable3"
                type="submit"
                value="Subscribe"
                onClick={sendDataToScript}
              >
                Subscribe
              </button>
              <div className="notice-text">
                <span>*</span>Your email is safe with us, we don't spam.
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => {
  return {
    loading: state.loading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setLoading: () => dispatch({ type: "set_loading" })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Loading);
// export default Loading;
