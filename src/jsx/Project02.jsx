import React, { Component } from "react";
import "../css/common.css";
import project02css from "../css/project02.css";

import $ from "jquery";
import { divScroll } from "./Animation";
import Background from "./Background";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
let home =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563779988/Devomark/home_t1oxyc.png";
class Project02 extends Component {
  state = {};
  componentDidMount() {
    divScroll(".client-img");
  }
  //   componentDidMount() {
  //     entryanimation("project", "01");
  //   }
  componentDidUpdate() {
    divScroll(".client-img");
  }

  render() {
    const renderClients = () => {
      // alert("this.props.activeClient" + this.props.activeClient);

      return this.props.projects.map((item, index) => {
        if (
          item.category == this.props.category &&
          item.client_name == this.props.activeClient
        ) {
          return (
            <div className="client-cont" key={index}>
              <div className="title-container animated fadeInUp animation">
                <div className="client-logo">
                  <img className="client-logo" src={item.client_logo} alt="" />
                </div>
                <div className="client-name">{item.client_name}</div>
              </div>
              <div className="detail-container animated fadeInRight animation">
                <div className="client-img" id="clientimgg">
                  <div className="client-images">
                    <div className="img-adjust">
                      <img
                        className="img-1"
                        src={item.client_img_desk}
                        alt=""
                      />
                    </div>
                    <div className="img-adjust">
                      <img
                        className="img-1"
                        src={item.client_img_mobile}
                        alt=""
                      />
                    </div>

                    {/* <img className="tab-img" src={item.client_img_tab} alt="" /> */}

                    {(() => {
                      if (!item.client_img_tab) return <React.Fragment />;
                      else
                        return (
                          <div className="img-adjust">
                            <img
                              className="img-1"
                              src={item.client_img_tab}
                              alt=""
                            />
                          </div>
                        );
                    })()}

                    {(() => {
                      if (!item.client_img_desk2) return <React.Fragment />;
                      else
                        return (
                          <div className="img-adjust">
                            <img
                              className="img-1"
                              src={item.client_img_desk2}
                              alt=""
                            />
                          </div>
                        );
                    })()}
                    {(() => {
                      if (!item.client_img_mobile2) return <React.Fragment />;
                      else
                        return (
                          <div className="img-adjust">
                            <img
                              className="img-1"
                              src={item.client_img_mobile2}
                              alt=""
                            />
                          </div>
                        );
                    })()}
                    {(() => {
                      if (!item.client_img_tab2) return <React.Fragment />;
                      else
                        return (
                          <div className="img-adjust">
                            <img
                              className="img-1"
                              src={item.client_img_tab2}
                              alt=""
                            />
                          </div>
                        );
                    })()}
                  </div>
                </div>

                {/* <div className="info-container">
                  <ul>
                    {item.client_work.map(work => (
                      <li>{work}</li>
                    ))}
                  </ul>
                </div> */}
                {/* <div className="mob-info-container animated fadeInDown animation">
                  <ul>
                    {item.client_work.map(work => (
                      <li>{work}</li>
                    ))}
                  </ul>
                </div> */}
              </div>
            </div>
          );
        }
      });
    };

    return (
      <div className={"project02-container " + this.props.className}>
        <Background
          sectionNumber="02"
          activeLink={this.props.activeLink}
          linkcount={this.props.linkcount}
          arrowHidden={true}
          description={"Our Project"}
        />
        <div className="dragto"> Drag To see more </div>
        <div className="case-study-container">
          {renderClients()}
          <div className="navigator animated fadeInDown animation">
            <div className="nav-name" onClick={() => this.props.prev()}>
              PREV
            </div>
            <NavLink activeClassName="activeRoute" to="/" exact>
              <img src={home} className="home-btn" />
            </NavLink>

            <div className="nav-name" onClick={() => this.props.next()}>
              NEXT
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    projects: state.projects,
    activeClient: state.activeClient,
    category: state.category
  };
};

const mapDispatchToProps = dispatch => {
  return {
    next: () => dispatch({ type: "next" }),
    prev: () => dispatch({ type: "prev" })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Project02);
