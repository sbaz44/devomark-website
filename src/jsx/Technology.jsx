import React, { Component } from "react";
import Background from "./Background";
import technologycss from "../css/technology.css";
import H1 from "./H1";
import H2 from "./H2";
import P from "./P";
import DataContainer from "./DataContainer";
import SoftwareIcon from "./SoftwareIcon";
import Browse from "./Browse";
import Icons from "./Icons";
import BrowseData from "./BrowseData";
import SideNav from "./SideNav";
import Clients from "./Clients";
import { connect } from "react-redux";
import { entryanimation } from "./Animation";
let illustrator =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563780957/Devomark/illustrator_ideaae.png";
let adobephoto =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563780956/Devomark/Adobe_Photo_i8qjhv.png";
let adobexd =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563780956/Devomark/Adobe_XD_zut9qj.png";
let zeplin =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563780956/Devomark/zeplin_axpvep.png";
let bgimage =
  "https://res.cloudinary.com/dxvkmplvo/image/upload/v1563779261/Devomark/home01-background_utdklg.png";

class Technology extends Component {
  state = {
    work: this.props.work,
    workk: this.props.work,
    titles: [
      {
        title: "UI/UX",
        // color: "black",
        paragraph:
          "If your website is the home for your digital presence, user experience (UI/UX) design is the floor plan, the structure, and the windows and doors. Everything that shapes how visitors experience and engage with you is defined by the blueprint that is UI/UX design. UX informs a site’s design in the same way that the layout of a room informs furnishings — it all comes down to functionality. You need to understand what people want to find and where they expect to find it before you can even consider colours, fonts, and photos. Our websites are designed to easily grow with your audience, business, and technology — without compromising the user experience or the structure.",
        imageSRC: [illustrator, adobephoto, adobexd, zeplin]
      },
      {
        title: "E-Commerce",
        // color: "red",
        paragraph:
          "E-commerceE-commerceE-commerceE-commerceE-commerceE-commerceE-commerceE-commerceE-commerceE-commerceE-commerceE-commerceE-commerceE-commerceE-commerceE-commerceE-commerceE-commerceE-commerceE-commerceE-commerceE-commerceE-commerceE-commerceE-commerceE-commerce",
        imageSRC: [illustrator, adobephoto, adobexd, zeplin]
      },
      {
        title: "Front End Development",
        // color: "yellow",
        paragraph:
          "Front end developmentFront end developmentFront end developmentFront end developmentFront end developmentFront end developmentFront end developmentFront end developmentFront end developmentFront end developmentFront end developmentFront end developmentFront end developmentFront end development",
        imageSRC: [illustrator, adobephoto, adobexd, zeplin]
      },
      {
        title: "Content Management System",
        // color: "green",
        paragraph:
          "Content Management SystemContent Management SystemContent Management SystemContent Management SystemContent Management SystemContent Management SystemContent Management SystemContent Management SystemContent Management SystemContent Management SystemContent Management SystemContent Management SystemContent Management SystemContent Management SystemContent Management SystemContent Management SystemContent Management SystemContent Management System",
        imageSRC: [illustrator, adobephoto, adobexd, zeplin]
      }
    ],
    links: [
      "UI/UX",
      "E-Commerce",
      "Front End Development",
      "Content Management System"
    ]
    // color: ["black", "yellow", "red", "blue"]
  };
  componentDidMount() {
    entryanimation("project", "01");
  }

  render() {
    const renderTech = () => {
      return this.state.titles.map(data => {
        if (data.title == this.props.category) {
          return (
            <React.Fragment>
              <Background
                sectionNumber="01"
                activeLink="link1"
                linkcount="2"
                description={data.title}
                arrowHidden={this.props.arrowHidden}
              />

              <DataContainer
                activeTitle={data.title}
                style={{ backgroundColor: data.color }}
              >
                <H1 className="title-text">{data.title}</H1>
                <P className="para-text">{data.paragraph}</P>
                <SoftwareIcon>
                  {/* <H2 className="tech-text">Technologies</H2> */}
                  <Icons src={data.imageSRC} />
                </SoftwareIcon>
                <Browse>
                  <H2 className="browse-text">Browse</H2>
                  <BrowseData
                    links={this.state.links}
                    activeLink={data.title}
                  />
                </Browse>
              </DataContainer>
            </React.Fragment>
          );
        }
      });
    };

    return (
      <div
        className={
          "technology-container project01-container" + this.props.className
        }
      >
        {renderTech()}

        {/* {this.state.title.map((data) =>
                                <H1>{data}</H1>
                            )} 
                            
                             <div style={{ backgroundColor: data.color }} className="uiux-container">
                            <H1>{data.title}</H1>
                        </div>
                            */}
        {/* {
                    this.state.color.map((index) =>
                        <div className="uiux-container">
                            <H1>{index}</H1>
                        </div>
                    )} */}

        {/* <div className="uiux-container">
                    <H1>UI/UX</H1>
                </div>*/}

        {/*
                
                version 2
                <div className="uiux-container">
                    <H1>UI/UX</H1>
                </div>
                <div className="uiux-container">
                    <div className="uiux-text">UI/UX</div>
                </div>
                <div className="uiux-container">
                    <div className="uiux-text">UI/UX</div>
                </div>
                <div className="uiux-container">
                    <div className="uiux-text">UI/UX</div>
                </div> */}
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    category: state.category
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Technology);
