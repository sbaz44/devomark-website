import $ from "jquery";
import commoncss from "../css/common.css";
import { swipe } from "jquery-touchswipe";
import { createBrowserHistory } from "history";
const history = createBrowserHistory();
let curr = "01";
let activeMainPage = "home";
let activeSection = "01";
let openClick = false;
export function checkOpenClick() {
  // alert("openClick" + openClick);
  return openClick;
}
export function changeOpenClick() {
  return !openClick;
}
export function exitanimation(activeMainPage) {
  $("." + activeMainPage + "" + curr + "-container .sectionNumber").css(
    "opacity",
    "0"
  );
  $("." + activeMainPage + "" + curr + "-container .quote-1").css(
    "opacity",
    "0"
  );
  $("." + activeMainPage + "" + curr + "-container .quote-2").css(
    "opacity",
    "0"
  );
  $("." + activeMainPage + "" + curr + "-container .line").css("width", "0");
  $("." + activeMainPage + "" + curr + "-container .section-number").css(
    "opacity",
    "0"
  );
  $(
    "." + activeMainPage + "" + curr + "-container .backgroundContainer img"
  ).css("opacity", "0");

  //setTimeout(function () {
  //}, 1000);
  setTimeout(function() {
    $("." + activeMainPage + "" + curr + "-container ").css("opacity", "0");
    $("." + activeMainPage + "" + curr + "-container ").css("display", "none");
  }, 100);
}
export function changecurrent(sectionNumber) {
  curr = "0" + parseInt(sectionNumber);
}

export function entryanimation(currentPage, sectionNumber) {
  activeMainPage = currentPage;
  activeSection = sectionNumber;
  // alert("currentPage " + activeMainPage + " activeSection " + activeSection);
  $(
    "." + activeMainPage + "" + "0" + parseInt(sectionNumber) + "-container "
  ).css("display", "block");

  setTimeout(function() {
    curr = "0" + parseInt(sectionNumber);
    $("." + activeMainPage + "" + curr + "-container ").css("display", "block");
    // alert("activeMainPage" + activeMainPage + "curr" + curr);

    $("." + activeMainPage + "" + curr + "-container .sectionNumber").css(
      "opacity",
      "0.28"
    );
    $("." + activeMainPage + "" + curr + "-container ").css("opacity", "1");
    $("." + activeMainPage + "" + curr + "-container ").css("opacity", "1");

    $("." + activeMainPage + "" + curr + "-container .quote-1").css(
      "opacity",
      "0.75"
    );
    $("." + activeMainPage + "" + curr + "-container .quote-2").css(
      "opacity",
      "1"
    );
    $("." + activeMainPage + "" + curr + "-container .section-number").css(
      "opacity",
      "1"
    );
    $(
      "." + activeMainPage + "" + curr + "-container .backgroundContainer img"
    ).css("opacity", "1");

    // $("." + activeMainPage + "" + curr + "-container .line").css(
    //   "width",
    //   128 * (100 / 1920) + "vw"
    // );
    // $("." + activeMainPage + "" + curr + "-container .line-custom").css(
    //   "width",
    //   38 * (100 / 1920) + "vw"
    // );
    //$(window).width() < 800
    if ($(window).width() < 800) {
      $("." + activeMainPage + "" + curr + "-container .line").css(
        "width",
        80 * (100 / 375) + "vw"
      );
      $("." + activeMainPage + "" + curr + "-container .line-custom").css(
        "width",
        33 * (100 / 375) + "vw"
      );
      $(
        "." +
          activeMainPage +
          "" +
          curr +
          "-container .active-line .line-custom"
      ).css("width", 67 * (100 / 375) + "vw");
    } else {
      $("." + activeMainPage + "" + curr + "-container .line").css(
        "width",
        128 * (100 / 1920) + "vw"
      );
      $("." + activeMainPage + "" + curr + "-container .line-custom").css(
        "width",
        38 * (100 / 1920) + "vw"
      );
      $(
        "." +
          activeMainPage +
          "" +
          curr +
          "-container .active-line .line-custom"
      ).css("width", 76 * (100 / 1920) + "vw");
    }
  }, 100);
}
export function nextPage(nextNumber) {
  activeSection = nextNumber;

  // alert(
  //   "activeMainPage" +
  //     activeMainPage +
  //     "curr" +
  //     curr +
  //     "activeSection " +
  //     activeSection
  // );
  //alert("entred");
  exitanimation(activeMainPage);
  entryanimation(activeMainPage, activeSection);

  if (activeMainPage === "home") {
    console.log("nextPage");
    if ($(".customCss1").css("display") === "block") {
      history.push("/home");
    }
    if ($(".customCss2").css("display") === "block") {
      history.push("/offer");
    }
    if ($(".customCss3").css("display") === "block") {
      history.push("/process");
    }
    if ($(".customCss4").css("display") === "block") {
      history.push("/clients");
    }
    if ($(".customCss5").css("display") === "block") {
      history.push("/locate");
    }
    if ($(".customCss6").css("display") === "block") {
      history.push("/portfolio");
    }
  }
}

export function next() {
  let maxPages = 6;
  // alert("Next Called");
  // alert("activeMainPage" + activeMainPage)
  if (activeMainPage === "home") {
    maxPages = 6;
    if ($(".customCss1").css("display") === "block") {
      history.push("/home");
    }
    if ($(".customCss2").css("display") === "block") {
      history.push("/offer");
    }
    if ($(".customCss3").css("display") === "block") {
      history.push("/process");
    }
    if ($(".customCss4").css("display") === "block") {
      history.push("/clients");
    }
    if ($(".customCss5").css("display") === "block") {
      history.push("/locate");
    }
    if ($(".customCss6").css("display") === "block") {
      history.push("/portfolio");
    }
    console.log("next()");
  }
  if (activeMainPage === "whyus") {
    maxPages = 2;
  }
  if (activeMainPage === "blog") {
    maxPages = 1;
  }
  if (activeMainPage === "contact") {
    maxPages = 1;
  }
  if (activeMainPage === "project") {
    maxPages = 1;
  }
  if (maxPages >= parseInt(curr) + parseInt(1)) {
    nextPage("0" + (parseInt(curr) + parseInt(1)));
  } else {
    //  alert("This is last page " + " parseInt(curr) + 1 " + (parseInt(curr) + parseInt(1)) + " maxPages " + maxPages);
  }

  // if ($(".customCss1").css("display") === "block") {
  //   return history.push("/home01");
  // } else
}
export function previous() {
  let maxPages = 6;
  if (activeMainPage === "home") {
    maxPages = 6;
    if ($(".customCss1").css("display") === "block") {
      history.push("/home");
    }
    if ($(".customCss2").css("display") === "block") {
      history.push("/offer");
    }
    if ($(".customCss3").css("display") === "block") {
      history.push("/process");
    }
    if ($(".customCss4").css("display") === "block") {
      history.push("/clients");
    }
    if ($(".customCss5").css("display") === "block") {
      history.push("/locate");
    }
    if ($(".customCss6").css("display") === "block") {
      history.push("/portfolio");
    }
    console.log("previous()");
  }
  if (activeMainPage === "whyus") {
    maxPages = 2;
  }
  if (activeMainPage === "blog") {
    maxPages = 1;
  }
  if (activeMainPage === "contact") {
    maxPages = 1;
  }
  if (activeMainPage === "project") {
    maxPages = 1;
  }
  if (1 <= parseInt(curr) - parseInt(1)) {
    nextPage("0" + (parseInt(curr) - parseInt(1)));
    // if ($(".customCss1").css("display") === "block") {
    //   history.push("/home");
    // }
    // if ($(".customCss2").css("display") === "block") {
    //   history.push("/offer");
    // }
    // if ($(".customCss3").css("display") === "block") {
    //   history.push("/process");
    // }
    // if ($(".customCss4").css("display") === "block") {
    //   history.push("/clients");
    // }
    // if ($(".customCss5").css("display") === "block") {
    //   history.push("/locate");
    // }
    // if ($(".customCss6").css("display") === "block") {
    //   history.push("/portfolio");
    // }
  } else {
    // alert("This is first page " + " parseInt(curr) + 1 " + (parseInt(curr) + parseInt(1)) + " maxPages " + maxPages);
  }
}

///////////////////////////

// var fired = 0;
// var displacement = 0;
// var d = 0;
// function scroll(e) {
//   d = e.originalEvent.wheelDelta;
//   d > 0 ? previous() : next();
// }

// $("body *:not('.home02-cont')").on("wheel", function(e) {
//   scroll(e);
// });

$("body").ready(function() {
  var number = $(".number");
  var i = 0;
  var sem = 0;
  var displacement = $(window).scrollTop();

  var scroll = 0;
  $(window).on("wheel", function(e) {
    if (sem == 0) {
      sem = 1;
      i++;
      console.log("Scrolll DATA " + e.originalEvent.wheelDelta);
      e.originalEvent.wheelDelta >= 0 ? previous() : next();

      $.data(
        this,
        "scrollTimer",
        setTimeout(function() {
          sem = 0;
          $(window).scrollTop($(window).scrollTop(e));
        }, 1000)
      );
    }
  });
});

$(document).ready(function() {
  $(document).on("keydown", function(e) {
    if (e.which == "38") {
      previous();
    }
    if (e.which == "40") {
      next();
    }
  });
});

// $(function() {
//   $("body *:not('.home02-cont')").swipe({
//     //Generic swipe handler for all directions
//     swipe: function(
//       event,
//       direction,
//       distance,
//       duration,
//       fingerCount,
//       fingerData
//     ) {
//       //  $(this).text("You swiped " + direction);
//       // alert("You swiped " + direction);
//       if (direction === "up") {
//         next();
//       }
//       if (direction === "down") {
//         previous();
//       }
//     }
//   });

//   //Set some options later
//   $("body *:not('.home02-cont')").swipe({ fingers: 1 });
// });

// $("body *:not('.home02-cont')").touchwipe({
//   wipeUp: function() {
//     previous();
//   },
//   wipeDown: function() {
//     next();
//   },
//   min_move_x: 20,
//   min_move_y: 20,
//   preventDefaultEvents: false
// });

export function divScroll(divname) {
  const sliderr = document.querySelectorAll(divname);
  // const slider = $(".client-img");
  //   alert(divname);
  sliderr.forEach(function(nodee) {
    let isDown = false;
    let startX;
    let scrollLeft;
    nodee.addEventListener("mousedown", e => {
      // alert("hi");
      e.preventDefault();
      isDown = true;
      nodee.classList.add("activediv");
      startX = e.pageX - nodee.offsetLeft;
      scrollLeft = nodee.scrollLeft;

      nodee.style.cursor = "grabbing";
      // console.log(isDown);
    });

    nodee.addEventListener("mouseleave", e => {
      e.preventDefault();
      isDown = false;
      nodee.style.cursor = "pointer";
      nodee.classList.remove("activediv");
    });
    nodee.addEventListener("mouseup", () => {
      isDown = false;
      nodee.style.cursor = "pointer";
      nodee.classList.remove("activediv");
    });

    nodee.addEventListener("mousemove", e => {
      if (!isDown) return;
      e.preventDefault();
      nodee.style.cursor = "grabbing";
      const x = e.pageX - nodee.offsetLeft;

      const walk = (x - startX) * 3; //scroll-fast
      if (walk === 0) {
        //  console.log("x " + walk + "No Drag");
        changeOpenClick();
      }
      nodee.scrollLeft = scrollLeft - walk;
      //   console.log(x);
    });
  });
}

export function VerticalScroll(divname) {
  const slider = document.querySelector(divname);
  let isDown = false;
  let startX;
  let scrollLeft;

  slider.addEventListener("mousedown", e => {
    isDown = true;
    // slider.classList.add("active");
    startX = e.pageY - slider.offsetTop;
    scrollLeft = slider.scrollTop;
    slider.style.cursor = "grabbing";
  });
  slider.addEventListener("mouseleave", () => {
    isDown = false;
    slider.style.cursor = "pointer";

    // slider.classList.remove("active");
  });
  slider.addEventListener("mouseup", () => {
    isDown = false;
    slider.style.cursor = "pointer";

    // slider.classList.remove("active");
  });
  slider.addEventListener("mousemove", e => {
    if (!isDown) return;
    slider.style.cursor = "grabbing";

    e.preventDefault();
    const x = e.pageY - slider.offsetTop;
    const walk = (x - startX) * 3; //scroll-fast
    slider.scrollTop = scrollLeft - walk;
    console.log(walk);
  });
}
