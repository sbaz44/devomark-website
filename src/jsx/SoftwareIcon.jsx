import React, { Component } from 'react';
import "../css/softwareicon.css";
class SoftwareIcon extends Component {
    state = {  }
    render() { 
        return ( 
            <div className={"softwareicon-container " + this.props.className}>
                {this.props.children}
            </div>
         );
    }
}
 
export default SoftwareIcon;