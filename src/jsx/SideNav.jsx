import React, { Component } from 'react';
import Navlinks from "./Navlinks";
import sidenavcss from "../css/sidenav.css";
import $ from "jquery";
class SideNav extends Component {
    state = {  }
    closeSidenav(){
        $(".sidenav-container").css('display','none');
    }
    render() { 
        return ( 
            <div className={"sidenav-container " + this.props.className}>
                <div className="close-text" onClick={e => this.closeSidenav(e)}>CLOSE</div>
                <Navlinks activeLink={this.props.activeLink}/>
            </div>
         );
    }
}
 
export default SideNav;