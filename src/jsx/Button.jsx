import React, { Component } from "react";
import "../css/button.css";

class Button extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div
        className={
          "button animated fadeInDown animation " + this.props.className
        }
        onClick={this.props.onClick}
      >
        <div className="buttonText">{this.props.text}</div>
      </div>
    );
  }
}

export default Button;
