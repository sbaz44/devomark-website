import React, { Component } from "react";
import "../css/locationInfo.css";

class LocationInfo extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div className={"LocationInfo " + this.props.className}>
        {(() => {
          if (!this.props.maplink)
            return (
              <React.Fragment>
                <div className="logo">
                  <img className="img img-adjust" src={this.props.img} alt="" />
                </div>
                <div className="text">{this.props.text}</div>
              </React.Fragment>
            );
          else if (
            this.props.maplink == "https://maps.app.goo.gl/HAwmAWBwJ8PJJfYa6"
          )
            return (
              <React.Fragment>
                <div className="logo">
                  <img className="img img-adjust" src={this.props.img} alt="" />
                </div>
                <a className="text" href={this.props.maplink} target="_blank">
                  {this.props.text}
                </a>
              </React.Fragment>
            );
        })()}

        {/* <a className="text" href={this.props.maplink} target="_blank">
          {this.props.text}
        </a> */}
      </div>
    );
  }
}

export default LocationInfo;
